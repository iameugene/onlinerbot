import json
import logging
import time
import urllib.request
from urllib.error import URLError

import requests

import initdata

ONLINER_API_URL = 'https://b2bapi.onliner.by'
ONLINER_B2B_AUTH_URL = 'https://b2bapi.onliner.by/oauth/token'
CLIENT_ID = '0db50c7d38e3f02165ae'
CLIENT_SECRET = '6446c7eaf305bf9cb825fcb5878b40e9b2861931'
_token = None


def send_request(url, headers=initdata.headers, num_retries=2, time_out=3, json_response=False):
    logging.info('Send request: %s', url)
    request = urllib.request.Request(url, headers=headers)
    try:
        response = urllib.request.urlopen(request).read()
    except URLError as e:
        print('fetcher.py -> send_request: ', e.reason)
        logging.warning('fetcher.py -> send_request Err: %s', e.reason)

        response = None
        if num_retries > 0:
            if hasattr(e, 'code') and 500 <= e.code < 600:
                print('Retry after {} sec.'.format(time_out))
                logging.warning('%s. \n Попытка %s ', e.reason, num_retries)
                time.sleep(time_out)
                # повторная попытка при 5XX HTTP errors
                return send_request(url, headers, num_retries-1, time_out)
    if not json_response:
        return response
    else:
        json_response = None
        try:
            json_response = json.loads(response.decode())
        except Exception as e:
            print('fetcher.py -> send_request ', e)
            logging.warning('Неудачная попытка преобразовать в json %s', e)
        return json_response


def _get_onliner_api_token():
    """
    Return auth token
    :return: str
    """
    global _token
    if _token:
        return _token
    try:
        logging.info('Получение токена')
        response = requests.post(ONLINER_B2B_AUTH_URL,
                                 data={'grant_type': 'client_credentials'},
                                 headers={'Accept': 'application/json'},
                                 auth=(CLIENT_ID, CLIENT_SECRET)).json()
        token = response['access_token']
        logging.info('Токен получен')
    except Exception as e:
        token = None
        print("_get_onliner_api_token() Error: ", e)
        logging.error('Токен не получен %s', e)
    _token = token
    return token


def onliner_api(method, path, data={}):
    """
    :param method: str (HTTP method GET|POST|PATCH|DELETE|PUT)
    :param path: str (URL PATH)
    :param data: dict (payload data)
    :return: dict (json object)
    """
    url = ONLINER_API_URL + path
    headers = {'Accept': 'application/json',
               'Authorization': 'Bearer ' + _get_onliner_api_token(),
               'Content-Type': 'application/json'}
    response = None
    try:
        response = requests.__getattribute__(method)(url, headers=headers,
                                                     data=data)
        response = response.json()
    except Exception as e:
        logging.error('onliner_api Err: %s', e)
    return response
