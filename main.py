import sys

import openpyxl
from openpyxl import Workbook

from onliner import get_min_price


def main(file):
    wb = openpyxl.load_workbook(file)
    print('opened')
    ws = wb.active
    dfile = 'out.xlsx'
    new_wb = Workbook()
    new_ws = new_wb.active
    # new_rows = []
    for row in ws.rows:
        if row[0].value is not None:
            # request_key с какой колонки в прайсе брать запрос онлайнер
            request_key = str(row[0].value)
            # price цена с онлйнера. name - то что спарсилось на онлайнере
            price, name = get_min_price(request_key)
            new_ws.append([row[0].value, name, price])
    new_wb.save(filename=dfile)
    # return new_rows

if __name__ == '__main__':
    file = sys.argv[1]
    main(file)


