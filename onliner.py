import logging

from fetcher import send_request
from fetcher import onliner_api
from helper import clear_non_ascii

ONLINER_QUERY_URL = 'https://catalog.api.onliner.by/search/products?query='
_token = None


def get_min_price(key, is_ascii=True):
    """
    Request onliner. Return tuple of
    minimal price and full name of requested product.
    It is not possible to get exactly needed product.
    Therefore function returns first product of returned
    list.
    :param key: str
    :param is_ascii: boolean
    :return: (int, str)
    """
    if is_ascii:
        key = clear_non_ascii(key)

    request_string = ONLINER_QUERY_URL + key
    response = send_request(request_string, json_response=True)

    min_price = None
    name = None
    try:
        products = response.get('products')
        first_product = products[0]
        min_price = first_product.get('prices').get('min')
        name = first_product.get('full_name')
    except Exception as e:
        print(e, " Can't get a price.")
        logging.error('Can\'t get a price. %s', e)
    return min_price, name


def get_offers(position_url):
    """
    Take a  product's position_url
     Return list of offers.

    :param position_url: str
    :return: list
    """
    response = send_request(position_url, json_response=True)
    offers = None
    try:
        offers = response['positions']['primary']
    except Exception as e:
        print("get_offers ", e)
        logging.error('get_offers. in onliner.py %s', e)
    return offers


def get_shop_prices(position_url):
    """
    Take a url from onliner. For example:
    'https://shop.api.onliner.by/products/aquapetfamily/positions'
    Return list of tuples (price, shop_id)
    :param position_url: str
    :return: list
    """
    offers = get_offers(position_url)
    shop_prices = []
    if isinstance(offers, list):
        for offer in offers:
            try:
                price = float(offer['position_price']['converted']['BYN']['amount'])
                shop_id = str(offer['shop_id'])
            except Exception as e:
                print("get_shop_prices ", e)
                logging.error('from function get_shop_prices() in onliner.py. %s', e)
                shop_prices.append([None, None])
            else:
                shop_prices.append((price, shop_id))

    return shop_prices


def get_user_offers():
    # TODO: get only active position
    """
    Return list of both activated and deactivated user's offers from  onliner api
    :return: json object
    """
    print("GET pokupka's offers")
    return onliner_api('get', '/positions')


def set_user_offers(offers):
    """
    Upload list of offers into Onliner
    Return Onliner response
    :param offers: json
    :return: None
    """
    print("SET pokupka's offers")
    onliner_api('patch', '/pricelists', offers)
