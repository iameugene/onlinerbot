def is_ascii(s):
    """
    проверяет строку на принадлежность ASCII
    """
    return all(ord(c) < 128 for c in s)


def clear_non_ascii(s):
    """
    очищает от символов не входящих в набор ASCII
    """
    return ''.join([i if ord(i) < 128 else '' for i in s])
