import csv
import json
import logging

from onliner import (get_shop_prices, get_user_offers, set_user_offers)

logging.basicConfig(filename='bot.log', level=logging.WARNING,
                    format='%(asctime)s %(levelname)s %(message)s')

DB_FILE = 'tracked_positions.csv'


_rule = {'min_price': 'True',
         'shift': .01,
         'total_in_min': 2,  # Amount of min price include user's price.
         'excluded_shop': []}
user_offers = None

if not user_offers:
    user_offers = get_user_offers()


def get_tracked_positions(file=DB_FILE):
    with open(file) as f:
        tracked_positions = csv.DictReader(f)
        return [tpos for tpos in tracked_positions]


def get_user_price(position):
    global user_offers
    price = None
    for offer in user_offers:
        if offer['model'] == position:
            price = offer['price']
            break
    return price


def update_price(position, new_price):
    global user_offers
    for offer in user_offers:
        if offer['model'] == position['name']:
            offer['price'] = '{:.2f}'.format(new_price)


def calculate_new_price(position, rule=_rule):
    """

    :param position: dict
    :param rule: dict
    :return: str
    """
    new_price = None
    user_price = get_user_price(position['name'])

    try:
        user_price = float(user_price)
    except Exception as e:
        logging.error("User price is None. "
                      "Maybe a tracked name and the name from"
                      " onliner api don't match %s", e)

    stop_price = float(position['stopprice'])
    _prices = get_shop_prices(position['url'])
    prices = [mp[0] for mp in _prices]
    try:
        min_price = min(prices)
    except Exception as e:
        logging.error("Can't get a min price: %s", e)
        return None
    total = prices.count(min_price)

    if user_price:
        if rule['min_price']:
            if user_price > min_price:
                if not min_price < stop_price:
                    new_price = min_price - rule['shift']
            elif user_price == min_price:
                if not min_price < stop_price:
                    if total > rule['total_in_min']:
                        new_price = min_price - rule['shift']
            else:
                new_price = min_price - rule['shift']
        else:
            new_price = min_price  # If the strategy is not a minimum price

    if new_price == user_price:  # If there was any changes
        return None
    else:
        return new_price


def monitor():
    """
    1. Get tracked positions
    3. Analyze and calculate_new_price
    4. Update user price if not None from calculate_new_price
    5. Send updated price if any change
    :return: None
    """

    isupdated = False
    tracked_positions = get_tracked_positions()
    for tracked_position in tracked_positions:
        if tracked_position['name'][0] is "#":
            continue
        new_price = calculate_new_price(tracked_position)
        if new_price:
            if isupdated is not True:
                isupdated = True
            print('update new price {} {}'.format(tracked_position['name'], new_price))
            logging.info('update new price %s %s', tracked_position['name'], new_price)
            update_price(tracked_position, new_price)
    if isupdated:
        upd_user_offers = json.dumps(user_offers)
        set_user_offers(upd_user_offers)


def main():
    monitor()

if __name__ == "__main__":
    # logging.basicConfig(filename='bot.log', level=logging.INFO,
    #                     format='%(asctime)s %(levelname)s %(message)s')
    main()
